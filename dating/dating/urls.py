"""dating URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from core import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.IndexView.as_view()),
    path('signout/', views.LogoutView.as_view()),
    path('signup/', views.SignupView.as_view()),
    path('__debug__/', include('debug_toolbar.urls')),
    path('profile/<int:id>', views.ProfileView.as_view()),
    path('profile/<int:id>/edit/', views.ProfileEditView.as_view()),
    path('photo/add', views.ProfilePhotoView.as_view()),
    path('chats/', views.MessageIndex.as_view()),
    path('messages/<int:id>/', views.MessageWriteView.as_view()),
    path('profile/<int:id>/complain', views.ComplaintAddView.as_view()),
    path('profile/remove_photos/', views.ProfilePhotoRemoveView.as_view()),
    path('chat/<int:id>/remove/', views.ChatRemoveView.as_view()),
    path('search/', views.SearchView.as_view()),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
