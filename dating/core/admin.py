from django.contrib import admin
from core.models import NAMES_OF_MODELS
from django.contrib.auth.admin import UserAdmin
from core.models import DatingUser, Complaint, Interest
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


@admin.register(DatingUser)
class UserProfile(UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined", 'ban_date')}),
        (_("Additional info"), {"fields": ("avatar", "gender", "searching_gender", "interests", "age")}),
        (_('Other'), {'fields': ('is_banned',)}),
    )
    # headers in users table
    list_display = ('username', 'email', 'is_banned', 'ban_date')

    # disabled to edit by admin
    readonly_fields = ('username', 'last_login', 'date_joined', 'avatar', 'ban_date')

    # filters for users table
    list_filter = ('is_banned', 'is_active',)

    actions = ['ban', 'unban']

    def ban(self, request, queryset):
        queryset.filter(is_banned=False).update(is_banned=True, ban_date=timezone.now())

    def unban(self, request, queryset):
        queryset.filter(is_banned=True).update(is_banned=False, ban_date=None)


@admin.register(Complaint)
class ComplaintAdmin(admin.ModelAdmin):
    list_display = ('sender', 'accused', 'created_at', 'handled', 'handle_date')
    list_filter = ('handled',)
    readonly_fields = ('handle_date', 'sender', 'accused')
    actions = ['handle', 'unhandle']

    def handle(self, request, queryset):
        queryset.filter(handled=False).update(handled=True, handle_date=timezone.now())

    def unhandle(self, request, queryset):
        queryset.filter(handled=True).update(handled=False, handle_date=None)


@admin.register(Interest)
class Interest(admin.ModelAdmin):
    list_display = ('name', 'category')

    list_filter = ('category',)


for model in NAMES_OF_MODELS:
    admin.site.register(model)

