from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages


class MyLoginRequiredMixin(LoginRequiredMixin):
    login_alert_message = None

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            if self.login_alert_message:
                messages.info(request, self.login_alert_message)
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
