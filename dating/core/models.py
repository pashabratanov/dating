from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth import get_user_model


class TimeIt(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


def user_upload_to(obj, file_name):
    return f'users/{obj.id}/{file_name}'


class DatingUser(AbstractUser):
    GENDERS = (
        ('male', 'male'),
        ('female', 'female')
    )

    email = models.EmailField(max_length=255, unique=True)
    avatar = models.ImageField(blank=True, null=True, upload_to=user_upload_to)
    gender = models.CharField(max_length=255, choices=GENDERS)
    searching_gender = models.CharField(max_length=255, choices=GENDERS)
    interests = models.ManyToManyField('core.Interest', related_name='users', blank=True)
    age = models.IntegerField(default=18, validators=[MinValueValidator(limit_value=18), MaxValueValidator(limit_value=100)])
    is_banned = models.BooleanField(default=False)
    ban_date = models.DateTimeField(null=True, blank=True)


class Chat(TimeIt):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Message(TimeIt):
    text = models.CharField(max_length=255)
    sender = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='message_sender')
    receiver = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='message_receiver')
    read = models.BooleanField(default=False)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name='messages')

    def __str__(self):
        return f'sender: {self.sender}, receiver: {self.receiver}'


def profile_photos_upload_to(obj, file_name):
    return f'users/{obj.owner.id}/photos/{file_name}'


class ProfilePhoto(TimeIt):
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='photos')
    image = models.ImageField(null=True, upload_to=profile_photos_upload_to)

    def __str__(self):
        return f'{self.owner}: {self.image.url}'


class Complaint(TimeIt):
    description = models.CharField(max_length=255)
    handle_date = models.DateTimeField(null=True, blank=True)
    sender = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='claim_sender')
    accused = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='claim_receiver')
    handled = models.BooleanField(default=False)

    def __str__(self):
        return f'sender: {self.sender}, accused: {self.accused}. created: {self.created_at}'


class Category(TimeIt):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Interest(TimeIt):
    name = models.CharField(max_length=255)
    category = models.ForeignKey('core.Category', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


NAMES_OF_MODELS = [Chat, Message, ProfilePhoto, Category]
