from django.contrib import auth
from django.contrib.auth import get_user_model
from django.test import TestCase, Client


class IndexViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_diagnostic(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<h1>recently joined</h1>')
        self.assertNotContains(response, '<h1>with similar interests</h1>')

    def test_diagnostic_not_found(self):
        response = self.client.get('/incorrect_url')
        self.assertEqual(response.status_code, 404)

    def test_diagnostic_after_login(self):
        self.user = get_user_model().objects.create(username='test')
        self.user.set_password('test')
        self.user.save()

        self.client.login(username='test', password='test')

        response = self.client.get('/')
        self.assertNotContains(response, '<h1>recently joined</h1>')
        self.assertContains(response, '<h1>with similar interests</h1>')

    def test_diagnostic_successful_login(self):
        self.user = get_user_model().objects.create(username='test')
        self.user.set_password('test')
        self.user.save()

        # self.client.login(username='test', password='test')

        self.client.post('/', {'username': 'test', 'password': 'test'})
        response = self.client.get('/')
        self.assertNotContains(response, '<h1>recently joined</h1>')
        self.assertContains(response, '<h1>with similar interests</h1>')

    def test_diagnostic_unsuccessful_login(self):
        self.user = get_user_model().objects.create(username='test')
        self.user.set_password('test')
        self.user.save()

        response = self.client.post('/', {'username': 'test', 'password': 'incorrect pass'})
        self.assertContains(response, '<li>Incorrect username/password</li>')


class LogoutViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = get_user_model().objects.create(username='test')
        self.user.set_password('test')
        self.user.save()

    def test_successful_signout(self):
        self.client.login(username='test', password='test')
        self.assertEqual(auth.get_user(self.client), self.user)
        self.client.get('/signout/')
        self.assertEqual(str(auth.get_user(self.client)), 'AnonymousUser')
        response = self.client.get('/')
        self.assertContains(response, '<h1>recently joined</h1>')


class SignupViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_diagnostic_signup(self):
        response = self.client.get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_successful_signup(self):
        self.assertEqual(get_user_model().objects.all().count(), 0)
        self.client.post('/signup/', {'username': 'test', 'password': 'G?1wR37a', 'check_password': 'G?1wR37a',
                                      'email': 'mail@example.com'})
        self.assertEqual(get_user_model().objects.all().count(), 1)

    def test_unsuccessful_signup(self):
        response = self.client.post('/signup/', {'username': 'test', 'password': 'G?1wR37a', 'check_password': 'tasdfat',
                                      'email': 'mail@example.com'})

        self.assertContains(response, '<div class="invalid-feedback">Passwords do not match:(</div>')


class ProfileViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = get_user_model().objects.create(username='test')
        self.user.set_password('test')
        self.user.save()

    def test_diagnostic_not_signin(self):
        response = self.client.get('/profile/1', follow=True)
        self.assertContains(response, 'You have no right to view any profile without login')

    def test_diagnostic_signin(self):
        self.client.login(username='test', password='test')
        self.assertEqual(auth.get_user(self.client), self.user)
        response = self.client.get('/profile/1')
        self.assertContains(response, '<h1>test</h1>')
