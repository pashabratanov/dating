from django import forms
from django.contrib.auth import get_user_model, authenticate, login

from core.models import Message, Complaint
from django.http import HttpResponseRedirect
from django.contrib import messages

class SignupForm(forms.ModelForm):
    password = forms.CharField(widget=forms.widgets.PasswordInput)
    check_password = forms.CharField(widget=forms.widgets.PasswordInput)

    class Meta:
        model = get_user_model()
        fields = ('username', 'email')

    def clean_check_password(self):
        if self.cleaned_data['password'] != self.cleaned_data['check_password']:
            raise forms.ValidationError('Passwords do not match:(')
        return self.cleaned_data['check_password']

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user


class SigninForm(forms.Form):
    username = forms.CharField(max_length=255)
    password = forms.CharField(widget=forms.widgets.PasswordInput)

    def clean(self):
        user = get_user_model().objects.filter(username=self.cleaned_data['username']).first()

        if not user or not user.check_password(self.cleaned_data['password']):
            raise forms.ValidationError('Incorrect username/password')

    def auth(self, request):
        user = authenticate(request, **self.cleaned_data)
        if user.is_banned:
            return HttpResponseRedirect('/')
        else:
            login(request, user)
        return user


class ProfileForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ('avatar', 'email', 'first_name', 'last_name', 'age', 'gender', 'searching_gender', 'interests')


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('text',)
        widgets = {
            'text': forms.Textarea(attrs={'cols': 15, 'rows': 20}),
        }

    def save(self, commit=True, **kwargs):
        message = super().save(commit=False)
        message.receiver = kwargs.pop('receiver')
        message.sender = kwargs.pop('sender')
        message.save()


class ComplaintForm(forms.ModelForm):
    def __init__(self, **kwargs):
        self.accused = kwargs.pop('accused')
        self.sender = kwargs.pop('sender')
        super().__init__(**kwargs)

    class Meta:
        model = Complaint
        fields = ['description']
        widgets = {
            'description': forms.Textarea(attrs={'cols': 15, 'rows': 20}),
        }

    def save(self, commit=True, **kwargs):
        instance = super().save(commit=False)
        instance.accused = self.accused
        instance.sender = self.sender
        return instance.save()
