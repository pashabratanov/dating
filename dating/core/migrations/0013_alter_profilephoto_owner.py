# Generated by Django 4.0.5 on 2022-07-06 13:20

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_remove_profilephoto_url_profilephoto_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profilephoto',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='photos', to=settings.AUTH_USER_MODEL),
        ),
    ]
