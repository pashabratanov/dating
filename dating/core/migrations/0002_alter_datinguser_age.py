# Generated by Django 4.0.5 on 2022-06-21 11:01

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datinguser',
            name='age',
            field=models.IntegerField(default=18, validators=[django.core.validators.MinValueValidator(limit_value=18), django.core.validators.MaxValueValidator(limit_value=100)]),
        ),
    ]
