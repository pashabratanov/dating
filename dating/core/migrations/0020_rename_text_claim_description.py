# Generated by Django 4.0.5 on 2022-07-19 10:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0019_alter_claim_handle_date'),
    ]

    operations = [
        migrations.RenameField(
            model_name='claim',
            old_name='text',
            new_name='description',
        ),
    ]
