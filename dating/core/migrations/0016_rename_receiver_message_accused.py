# Generated by Django 4.0.5 on 2022-07-19 10:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_datinguser_is_banned'),
    ]

    operations = [
        migrations.RenameField(
            model_name='message',
            old_name='receiver',
            new_name='accused',
        ),
    ]
