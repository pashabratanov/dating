from django import template

register = template.Library()

@register.filter
def to_strftime(date, date_format="%m/%d/%Y, %H:%M:%S"):
    return date.strftime(date_format)

@register.filter
def has_photos(user):
    return user.photos.all().count() > 0
