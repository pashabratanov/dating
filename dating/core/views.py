
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, View, FormView, DetailView, ListView
from django.contrib.auth import logout, get_user_model
from django.shortcuts import redirect, get_object_or_404
from core import forms
from core.models import ProfilePhoto, Chat, Message
from core.custom_mixins import MyLoginRequiredMixin
from django.db.models import Q


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'dating'
        context['form'] = forms.SigninForm

        if self.request.user.is_authenticated:
            context['users'] = get_user_model().objects\
                .filter(interests__in=self.request.user.interests.all(), gender=self.request.user.searching_gender)\
                .distinct()

            context['users_title'] = 'with similar interests'
        else:
            context['users_title'] = 'recently joined'
            context['users'] = get_user_model().objects.all()[:10]

        return context

    def post(self, request):
        form = forms.SigninForm(data=request.POST)

        if form.is_valid():
            form.auth(request)
            return redirect('/')
        return self.render_to_response(context={'form': form})


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')


class SignupView(FormView):
    template_name = 'signup.html'
    form_class = forms.SignupForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'signup'
        return context

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class ProfileView(MyLoginRequiredMixin, DetailView):
    template_name = 'profile.html'
    model = get_user_model()
    pk_url_kwarg = 'id'
    login_alert_message = 'You have no right to view any profile without login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.kwargs['id']
        context['title'] = 'profile'
        context['photos'] = kwargs['object'].photos.all()
        context['interests'] = kwargs['object'].interests.all()

        if self.request.session.get('photo_error', False):
            context['error'] = self.request.session.get('photo_error')
            del self.request.session['photo_error']

        return context


class ProfileEditView(MyLoginRequiredMixin, FormView):
    template_name = 'profile_edit.html'
    form_class = forms.ProfileForm
    login_alert_message = 'You have no right to edit profile without login'

    def get_success_url(self):
        return f'/profile/{self.profile_id}'

    def dispatch(self, *args, **kwargs):
        self.profile_id = kwargs['id']

        # if you try to edit not your profile it redirects to main page
        if self.request.user.id != self.profile_id:
            return HttpResponseRedirect('/')

        self.instance = get_object_or_404(get_user_model(), id=self.profile_id)
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'edit profile'
        return context

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['instance'] = self.instance
        return form_kwargs

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class ProfilePhotoView(MyLoginRequiredMixin, View):
    login_alert_message = 'You have no right to upload photo without login'
    limit = 20

    def get(self, *args, **kwargs):
        return redirect('/')

    def post(self, request):
        next_url = request.POST.get('next', '/')

        if request.user.photos.count() >= self.limit:
            request.session['photo_error'] = f'You cant have more than {self.limit} photos.'
            return redirect(next_url)

        image = request.FILES.get('img', None)

        if image and 'image' in image.content_type:
            pf = ProfilePhoto(owner=request.user, image=image)
            pf.save()
        else:
            request.session['photo_error'] = 'You can send only image files'

        return HttpResponseRedirect(next_url)


class MessageIndex(MyLoginRequiredMixin, TemplateView):
    template_name = 'messages.html'
    login_alert_message = 'You have no right to see messages without login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['chats'] = []
        chats = Chat.objects.filter(Q(messages__sender_id=self.request.user.id) | Q(messages__receiver_id=self.request.user.id)).distinct()
        for chat in chats:
            message = chat.messages.last()

            if message.sender == self.request.user:
                partner = message.receiver
            else:
                partner = message.sender
            context['chats'].append({'partner': partner, 'chat': chat, 'last_message': message})

        context['title'] = 'messages'
        return context


class MessageWriteView(MyLoginRequiredMixin, TemplateView):
    template_name = 'message.html'

    login_alert_message = 'You have no access to messages without login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sent_messages = Message.objects.filter(sender_id=self.request.user.id, receiver_id=kwargs['id'])
        received_messages = Message.objects.filter(sender_id=kwargs['id'], receiver_id=self.request.user.id)
        messages = sent_messages | received_messages
        context['messages'] = messages.order_by('created_at')

        context['other_user'] = get_object_or_404(get_user_model(), id=kwargs['id'])
        context['title'] = context['other_user'].username
        return context


class ComplaintAddView(MyLoginRequiredMixin, FormView):
    template_name = 'complaint.html'
    form_class = forms.ComplaintForm
    success_url = '/'
    login_alert_message = 'You have no right to write complaints without login'

    def dispatch(self, request, *args, **kwargs):
        self.accused = get_object_or_404(get_user_model(), id=self.kwargs['id'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['accused'] = self.accused
        return context

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['accused'] = self.accused
        form_kwargs['sender'] = self.request.user
        return form_kwargs


class ProfilePhotoRemoveView(MyLoginRequiredMixin, TemplateView):
    template_name = 'photos.html'
    login_alert_message = 'You have no right to remove photos without login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['photos'] = self.request.user.photos.all()
        return context

    def post(self, request):
        ids = request.POST.get('photo_ids', '')

        if ids != '':
            photos_to_delete = request.user.photos.filter(pk__in=ids.split(','))
            photos_to_delete.delete()
        return redirect('/profile/remove_photos/')


class ChatRemoveView(MyLoginRequiredMixin, View):
    login_alert_message = 'You have no right to remove chats without login'

    def get(self, request, id):
        chat = get_object_or_404(Chat, id=id)

        message = chat.messages.all().first()

        if message.sender.id == request.user.id or message.receiver.id == request.user.id:
            chat.delete()
        return HttpResponseRedirect('/chats/')


class SearchView(MyLoginRequiredMixin, ListView):
    model = get_user_model()
    login_alert_message = 'You can search without login'
    template_name = 'search_results.html'
    context_object_name = 'users'

    def get_queryset(self):
        query = self.request.GET.get('q', False)
        if query:
            return get_user_model()\
                .objects.filter(Q(username=query) | Q(interests__name=query))\
                .exclude(id=self.request.user.id).distinct()

        return super().get_queryset()


