$(document).ready(function(){
        let array = [];
          $(".col-md-2 img").click(function(){
                let id = $(this).data('id');
                $(this).parent().toggleClass('selected-img');

                if (!array.includes(id)) {
                    array.push(id)
                } else {
                    for(let i in array) {
                        if(array[i]==id){
                            array.splice(i, 1);
                            break;
                        }
                    }
                }
                $('#photo_ids').attr('value', array);

                len = array.length;

                if (array.length > 0) {
                    $('#remove_btn').text('remove ' + array.length)
                } else {
                    $('#remove_btn').text('remove')
                }

          });
        });