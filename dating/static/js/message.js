// id of the current user
const current_user_id = $('#current-user-id').val();
const other_user_id = $('#other-user-id').val();

// elements of page
const send_button = $('#send-button');
const input_area = $('#text-area');
const messages_area = $('#message-block');

// focus on input messages after loading the page
input_area.focus();


// instance of websocket
const ws = new WebSocket(`ws:${window.location.host}/messages/${other_user_id}/`);

// listens the socket on incoming messages
ws.onmessage = (event) => {
    data = JSON.parse(event.data);
    if (current_user_id == data.sender_id) {
        messages_area.append(message_object('me', data.time, data.message))
    } else {
        messages_area.append(message_object(data.username, data.time, data.message, 'senders-message'))
    }
}

// send text to socket
$('#send-button').click(function() {
    text = input_area.val();

    if (text.replace(/ /g,'') != '') {
       data = {'text': text, 'sender_id': current_user_id, 'receiver_id': other_user_id}
        ws.send(JSON.stringify(data));
        input_area.val('');
    }
});

// returns message div
const message_object = function(sender, time, text, className='my-message') {
    return `<div class="message ${className}">
                <b>${sender}:</b> ${time}
                <div>${text}</div>
            </div>`
};
