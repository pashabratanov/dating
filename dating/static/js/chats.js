console.log('chats js');

const remove_links = $('.delete-button');

const modal_title = $('#exampleModalLabel');
const yes_button = $('#yes-button');


remove_links.click(function() {
   let chat_id = $(this).data('chat-id');
   let partner = $(this).data('chat-partner');
   modal_title.text(`Remove chat with ${partner}?`);
    yes_button.attr('href', `/chat/${chat_id}/remove/`);
});
