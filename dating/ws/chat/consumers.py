import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404

from core.models import Chat, Message
from django.utils import timezone


# please run docker run -p 6379:6379 -d redis:5 for ChatConsumer
class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.create_chat()

        self.group_name = self.chat.name

        async_to_sync(self.channel_layer.group_add)(
            self.group_name, self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )

    def receive(self, **kwargs):
        text = kwargs['text_data']
        data = json.loads(text)

        current_user = get_object_or_404(get_user_model(), id=data['sender_id'])
        if data.get('receiver_id', False):
            Message.objects.create(text=data['text'], sender_id=data['sender_id'], receiver_id=data['receiver_id'], chat_id=self.chat.id)
        current_datetime = timezone.now().strftime("%m/%d/%Y, %H:%M:%S")
        async_to_sync(self.channel_layer.group_send)(
            self.group_name,
            {
                'type': 'chat_message',
                'message': data['text'],
                'username': current_user.username,
                'time': str(current_datetime),
                'sender_id': current_user.id
            }
        )

    def chat_message(self, event):
        self.send(json.dumps(event))

    def create_chat(self):
        self.receiver_id = self.scope['url_route']['kwargs']['id']
        self.sender_id = self.scope['user'].id
        lst = sorted([self.receiver_id, self.sender_id])
        chat_name = f'chat_{lst[0]}_{lst[1]}'

        self.chat, created = Chat.objects.get_or_create(name=chat_name)

