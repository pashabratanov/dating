from django.urls import path

from ws.chat.consumers import ChatConsumer

websocket_urlpatterns = [
    path('messages/<int:id>/', ChatConsumer.as_asgi()),
]